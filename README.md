# TaskManager.Api
RESTful API built with .Net 5.0 to provide endpoints for task management. Developed using maintainable architecture and authentication support.
## Frameworks and Libraries
- [.NET 5.0](https://docs.microsoft.com/en-us/dotnet/core/dotnet-five)
- [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/)
- [AutoMapper](https://automapper.org/)
- [Swashbuckle](https://github.com/domaindrivendev/Swashbuckle)
- [Entity Framework In-Memory Provider](https://docs.microsoft.com/en-us/ef/core/miscellaneous/testing/in-memory) (for testing)
- [Fluent Assertions](https://fluentassertions.com/)
## Endpoints
The API includes implementation of four controllers: Auth, Users, Task Boards and Assignments.

![Api swagger](/Images/endpointsPng.png)

## How to run
Make sure you have installed .Net 5.0
At the API root path run the following commands:

```
dotnet restore
dotnet run
```
then navigate to local url
```
https://localhost:5001/swagger/index.html
```
if you want to check if API is working you can navigate to:

```
https://localhost:5001/api/Users
```
to get list of users, or simply use external software like [Postman](https://www.postman.com/) to test all endpoints (remember to add bearer authentication token in request header)

Important

Database was generated with code-first approach using Entity Framework Code. Connection string is set in appsettings.json. You'll need to change connection string to your local Sql Server before executing ```update-database```. You can also change database provider e. g. to In memory database. Also you could store connection string in user's secret on your local machine (it's a change i am palnning to make in near future).

Also i'm planning to make client app to consume TaskManager.Api in Angular. It will be available as soon as it reaches MVP(-ish) state :)
