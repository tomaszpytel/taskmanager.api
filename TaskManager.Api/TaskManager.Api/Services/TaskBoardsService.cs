﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Repositories;
using TaskManager.Api.Domain.Repositories.UnitOfWork;
using TaskManager.Api.Domain.Services;
using TaskManager.Api.Domain.Services.Responses;

namespace TaskManager.Api.Services
{
    public class TaskBoardsService : ITaskBoardsService
    {
        private readonly ITaskBoardsRepository _taskBoardsRepository;
        private readonly IUnitOfWork _unitOfWork;

        public TaskBoardsService(ITaskBoardsRepository taskBoardsRepository, IUnitOfWork unitOfWork)
        {
            _taskBoardsRepository = taskBoardsRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<TaskBoardResponse> AddAsync(TaskBoard taskBoard)
        {
            try
            {
                await _taskBoardsRepository.AddAsync(taskBoard);
                await _unitOfWork.CompleteAsync();

                return new TaskBoardResponse(taskBoard);
            }
            catch (Exception ex)
            {
                return new TaskBoardResponse($"An error occurred when adding new taskboard data: {ex.Message}");
            }
        }

        public async Task<TaskBoardResponse> DeleteAsync(int id)
        {
            var existingTaskBoard = await _taskBoardsRepository.FindByIdAsync(id);

            if (existingTaskBoard == null)
                return new TaskBoardResponse("TaskBoard not found");

            try
            {
                _taskBoardsRepository.Remove(existingTaskBoard);
                await _unitOfWork.CompleteAsync();

                return new TaskBoardResponse(existingTaskBoard);
            }
            catch (Exception ex)
            {
                return new TaskBoardResponse($"An error occurred when removing taskboard: {ex.Message}");
            }
        }

        public Task<IEnumerable<TaskBoard>> ListAsync(int userId)
        {
            return _taskBoardsRepository.ListAsync(userId);
        }

        public async Task<TaskBoardResponse> UpdateAsync(int id, TaskBoard taskBoard)
        {
            var existingTaskBoard = await _taskBoardsRepository.FindByIdAsync(id);

            if (existingTaskBoard == null)
                return new TaskBoardResponse("TaskBoard not found");

            existingTaskBoard.Title = taskBoard.Title;
            if (taskBoard.Description != null)
                existingTaskBoard.Description = taskBoard.Description;

            try
            {
                _taskBoardsRepository.Update(existingTaskBoard);
                await _unitOfWork.CompleteAsync();

                return new TaskBoardResponse(existingTaskBoard);
            }
            catch (Exception ex)
            {
                return new TaskBoardResponse($"An error occurred during taskboard update: {ex.Message}");
            }
        }
    }
}
