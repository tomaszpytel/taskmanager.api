﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Repositories;
using TaskManager.Api.Domain.Repositories.UnitOfWork;
using TaskManager.Api.Domain.Services;
using TaskManager.Api.Domain.Services.Responses;

namespace TaskManager.Api.Services
{
    public class AssignmentsService : IAssignmentsService
    {
        private readonly IAssignmentsRepository _assignmentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public AssignmentsService(IAssignmentsRepository assignmentsRepository, IUnitOfWork unitOfWork)
        {
            _assignmentRepository = assignmentsRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<AssignmentResponse> AddAsync(Assignment assignment)
        {
            try
            {
                await _assignmentRepository.AddAsync(assignment);
                await _unitOfWork.CompleteAsync();

                return new AssignmentResponse(assignment);
            }
            catch (Exception ex)
            {
                return new AssignmentResponse($"An error occurred when adding new assignment data: {ex.Message}");
            }
        }

        public async Task<AssignmentResponse> DeleteAsync(int id)
        {
            var existingAssignment = await _assignmentRepository.FindByIdAsync(id);

            if (existingAssignment == null)
                return new AssignmentResponse("Assignemnt not found");

            try
            {
                _assignmentRepository.Remove(existingAssignment);
                await _unitOfWork.CompleteAsync();

                return new AssignmentResponse(existingAssignment);
            }
            catch (Exception ex)
            {
                return new AssignmentResponse($"An error occurred when removing assignment data: {ex.Message}");
            }
        }

        public Task<IEnumerable<Assignment>> ListAsync(int TaskBoardId)
        {
            return _assignmentRepository.ListAsync(TaskBoardId);
        }

        public async Task<AssignmentResponse> UpdateAsync(int id, Assignment assignment)
        {
            var existingAssignment = await _assignmentRepository.FindByIdAsync(id);

            if (existingAssignment == null)
                return new AssignmentResponse("Assignment not found");

            existingAssignment.Title = assignment.Title;
            if (assignment.Decsription != null)
                existingAssignment.Decsription = assignment.Decsription;

            try
            {
                _assignmentRepository.Update(existingAssignment);
                await _unitOfWork.CompleteAsync();

                return new AssignmentResponse(existingAssignment);
            }
            catch (Exception ex)
            {
                return new AssignmentResponse($"An error occurred during assignment update: {ex.Message}");
            }
        }
    }
}
