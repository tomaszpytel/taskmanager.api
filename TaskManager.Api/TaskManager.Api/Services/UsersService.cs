﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Repositories;
using TaskManager.Api.Domain.Repositories.UnitOfWork;
using TaskManager.Api.Domain.Services;
using TaskManager.Api.Domain.Services.Responses;

namespace TaskManager.Api.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUserRepository _usersRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPasswordHasher<User> _passwordHasher;

        public UsersService(IUserRepository usersRepository, IUnitOfWork unitOfWork, IPasswordHasher<User> passwordHasher)
        {
            _usersRepository = usersRepository;
            _unitOfWork = unitOfWork;
            _passwordHasher = passwordHasher;
        }

        public async Task<UserResponse> DeleteAsync(int id)
        {
            var existingUser = await _usersRepository.FindByIdAsync(id);

            if (existingUser == null)
                return new UserResponse("User not found");

            try
            {
                _usersRepository.Remove(existingUser);
                await _unitOfWork.CompleteAsync();

                return new UserResponse(existingUser);
            }
            catch (Exception ex)
            {
                return new UserResponse($"An error occurred when removing user: {ex.Message}");
            }
        }

        public Task<IEnumerable<User>> ListAsync()
        {
            return _usersRepository.ListAsync();
        }

        public async Task<UserResponse> AddAsync(User user)
        {
            var existingUser = await _usersRepository.FindByUsernameAsync(user.Username);
            if (existingUser != null)
                return new UserResponse("Username already exists");
            
            user.Role = EUserRole.Default;
            user.Password = _passwordHasher.HashPassword(user, user.Password);

            try
            {
                await _usersRepository.AddAsync(user);
                await _unitOfWork.CompleteAsync();

                return new UserResponse(user);
            }
            catch (Exception ex)
            {
                return new UserResponse($"An error occurred when adding new user data: {ex.Message}");
            }
        }

        public async Task<UserResponse> UpdateAsync(int id, User user)
        {
            var existingUser = await _usersRepository.FindByIdAsync(id);

            if (existingUser == null)
                return new UserResponse("User not found");

            existingUser.Username = user.Username;
            existingUser.Password = _passwordHasher.HashPassword(user, user.Password);

            try
            {
                _usersRepository.Update(existingUser);
                await _unitOfWork.CompleteAsync();

                return new UserResponse(existingUser);
            }
            catch (Exception ex)
            {
                return new UserResponse($"An error occurred during user update: {ex.Message}");
            }
        }

        public async Task<User> FindByUsernameAsync(string username)
        {
            return await _usersRepository.FindByUsernameAsync(username);
        }
    }
}
