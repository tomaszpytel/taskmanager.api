﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Security;
using TaskManager.Api.Domain.Services;
using TaskManager.Api.Domain.Services.Responses;

namespace TaskManager.Api.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUsersService _usersService;
        private readonly ITokenHandler _tokenHandler;
        private readonly IPasswordHasher<User> _passwordHasher;

        public AuthenticationService(IUsersService usersService, ITokenHandler tokenHandler, IPasswordHasher<User> passwordHasher)
        {
            _usersService = usersService;
            _tokenHandler = tokenHandler;
            _passwordHasher = passwordHasher;
        }

        public async Task<TokenResponse> GenerateAccessTokenAsync(string username, string password)
        {
            var user = await _usersService.FindByUsernameAsync(username);
            var passwordVerificationStatus = _passwordHasher.VerifyHashedPassword(user, user.Password, password);

            if (user == null || passwordVerificationStatus == PasswordVerificationResult.Failed)
                return new TokenResponse("Invalid credendials");

            var token = _tokenHandler.CreateAccessToken(user);
            token.Username = username;

            return new TokenResponse(token);
        }

        public async Task<TokenResponse> RefreshTokenAsync(string refreshToken, string username)
        {
            var token = _tokenHandler.TakeRefreshToken(refreshToken);

            if (token == null)
                return new TokenResponse("Invalid refresh token");

            if (token.IsExpired())
                return new TokenResponse("Token has expired");

            var user = await _usersService.FindByUsernameAsync(username);
            if (user == null)
                return new TokenResponse("Invalid user credentials");

            var accessToken = _tokenHandler.CreateAccessToken(user);

            return new TokenResponse(accessToken);
        }

        public void RevokeRefreshToken(string refreshToken)
        {
            _tokenHandler.RevokeRefreshToken(refreshToken);
        }
    }
}
