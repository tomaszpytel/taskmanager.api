﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace TaskManager.Api.Helpers
{
    public static class IdentityHelper
    {
        public static int GetUserId(HttpContext context)
        {
            try
            {
                return int.Parse(context.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            }
            catch (Exception ex)
            {
                throw new ArgumentOutOfRangeException($"Unable to find id in claims identity: {ex.Message}");
            }
        }
    }
}