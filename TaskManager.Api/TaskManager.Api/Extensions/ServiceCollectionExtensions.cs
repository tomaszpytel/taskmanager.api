﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Repositories;
using TaskManager.Api.Domain.Repositories.UnitOfWork;
using TaskManager.Api.Domain.Security;
using TaskManager.Api.Domain.Services;
using TaskManager.Api.Persistence.Contexts;
using TaskManager.Api.Persistence.Repositories;
using TaskManager.Api.Security.Tokens;
using TaskManager.Api.Services;

namespace TaskManager.Api.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDataAccessServices(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<TaskManagerContext>(options => 
                options.UseSqlServer(connectionString));

            return services;
        }

        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            //repositories
            services.AddScoped<IUserRepository, UsersRepository>();
            services.AddScoped<ITaskBoardsRepository, TaskBoardsRepository>();
            services.AddScoped<IAssignmentsRepository, AssignmentsRepository>();
            //services
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<ITaskBoardsService, TaskBoardsService>();
            services.AddScoped<IAssignmentsService, AssignmentsService>();

            return services;
        }

        public static IServiceCollection AddSecurityServices(this IServiceCollection services)
        {
            services.AddSingleton<ITokenHandler, TokenHandler>();
            services.AddSingleton<IPasswordHasher<User>, PasswordHasher<User>>();

            return services;
        }

        public static IServiceCollection AddJWTAuthentication(this IServiceCollection services, Security.Tokens.TokenOptions tokenOptions)
        {
            var signingConfigurations = new SigningConfigurations(tokenOptions.Secret);

            services.AddSingleton(signingConfigurations);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                    {
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = tokenOptions.Issuer,
                        ValidAudience = tokenOptions.Audience,
                        IssuerSigningKey = signingConfigurations.SecurityKey,
                        ClockSkew = TimeSpan.Zero
                    };
                });

            return services;
        }

        public static IServiceCollection BindOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<Security.Tokens.TokenOptions>(configuration.GetSection("TokenOptions"));

            return services;
        }
    }
}
