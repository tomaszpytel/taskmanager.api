﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Security;
using TaskManager.Api.Resources;

namespace TaskManager.Api.Mapping
{
    public class ResourceToModelProfile : Profile
    {
        public ResourceToModelProfile()
        {
            CreateMap<CreateUserResource, User>();

            CreateMap<AccessToken, AccessTokenResource>()
                .ForMember(a => a.AccessToken, opt => opt.MapFrom(a => a.Token))
                .ForMember(a => a.RefreshToken, opt => opt.MapFrom(a => a.RefreshToken.Token))
                .ForMember(a => a.Expiration, opt => opt.MapFrom(a => a.Expiration));

            CreateMap<CreateTaskBoardResource, TaskBoard>();

            CreateMap<CreateAssignmentResource, Assignment>();
        }
    }
}
