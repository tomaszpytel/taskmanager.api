﻿using AutoMapper;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Resources;

namespace TaskManager.Api.Mapping
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<User, UserResource>();

            CreateMap<TaskBoard, TaskBoardResource>();

            CreateMap<Assignment, AssignmentResource>();
        }
    }
}
