﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Services;
using TaskManager.Api.Extensions;
using TaskManager.Api.Helpers;
using TaskManager.Api.Resources;

namespace TaskManager.Api.Controllers
{
    [Route("/api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class TaskboardsController : Controller
    {
        private readonly ITaskBoardsService _taskBoardsService;
        private readonly IMapper _mapper;

        public TaskboardsController(ITaskBoardsService taskBoardsService, IMapper mapper)
        {
            _taskBoardsService = taskBoardsService;
            _mapper = mapper;
        }

        [Authorize]
        [HttpGet]
        public async Task<IEnumerable<TaskBoardResource>> ListAsync()
        {
            var currentUserId = IdentityHelper.GetUserId(HttpContext);

            var taskBoards = await _taskBoardsService.ListAsync(currentUserId);
            var resources = _mapper.Map<IEnumerable<TaskBoard>, IEnumerable<TaskBoardResource>>(taskBoards);

            return resources;
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] CreateTaskBoardResource createTaskBoardResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            var currentUserId = IdentityHelper.GetUserId(HttpContext);

            var taskBoard = _mapper.Map<CreateTaskBoardResource, TaskBoard>(createTaskBoardResource);
            taskBoard.UserId = currentUserId;

            var result = await _taskBoardsService.AddAsync(taskBoard);

            if (!result.Success)
                return BadRequest(result.Message);

            var taskBoardResource = _mapper.Map<TaskBoard, TaskBoardResource>(result.Resource);
            return Ok(taskBoardResource);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(int id, [FromBody] CreateTaskBoardResource createTaskBoardResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            var taskBoard = _mapper.Map<CreateTaskBoardResource, TaskBoard>(createTaskBoardResource);
            var result = await _taskBoardsService.UpdateAsync(id, taskBoard);

            if (!result.Success)
                return BadRequest(result.Message);

            var taskBoardResource = _mapper.Map<TaskBoard, TaskBoardResource>(result.Resource);

            return Ok(taskBoardResource);
        }

        [Authorize]
        [HttpDelete]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var result = await _taskBoardsService.DeleteAsync(id);

            if (!result.Success)
                return BadRequest(result.Message);

            var taskBoardResource = _mapper.Map<TaskBoard, TaskBoardResource>(result.Resource);
            return Ok(taskBoardResource);
        }
    }
}
