﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Services;
using TaskManager.Api.Extensions;
using TaskManager.Api.Resources;

namespace TaskManager.Api.Controllers
{
    [Route("/api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class AssignmentsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IAssignmentsService _assignmentsService;

        public AssignmentsController(IMapper mapper, IAssignmentsService assignmentsService)
        {
            _mapper = mapper;
            _assignmentsService = assignmentsService;
        }

        [Authorize]
        [HttpGet]
        public async Task<IEnumerable<AssignmentResource>> ListAsync(int TaskBoardId)
        {
            var assignments = await _assignmentsService.ListAsync(TaskBoardId);
            var resources = _mapper.Map<IEnumerable<Assignment>, IEnumerable<AssignmentResource>>(assignments);

            return resources;
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] CreateAssignmentResource createAssignmentResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            var assignment = _mapper.Map<CreateAssignmentResource, Assignment>(createAssignmentResource);
            var result = await _assignmentsService.AddAsync(assignment);

            if (!result.Success)
                return BadRequest(result.Message);

            var assignmentResource = _mapper.Map<Assignment, AssignmentResource>(result.Resource);
            return Ok(assignmentResource);
        }

        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(int id, [FromBody] CreateAssignmentResource createAssignmentResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            var assignment = _mapper.Map<CreateAssignmentResource, Assignment>(createAssignmentResource);
            var result = await _assignmentsService.UpdateAsync(id, assignment);

            if (!result.Success)
                return BadRequest(result.Message);

            var assignmentResource = _mapper.Map<Assignment, AssignmentResource>(result.Resource);
            return Ok(assignmentResource);
        }

        [Authorize]
        [HttpDelete]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var result = await _assignmentsService.DeleteAsync(id);

            if (!result.Success)
                return BadRequest(result.Message);

            var assignmentResource = _mapper.Map<Assignment, AssignmentResource>(result.Resource);
            return Ok(assignmentResource);
        }
    }
}
