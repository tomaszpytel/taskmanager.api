﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Security;
using TaskManager.Api.Domain.Services;
using TaskManager.Api.Extensions;
using TaskManager.Api.Resources;

namespace TaskManager.Api.Controllers
{
    [ApiController]
    public class AuthController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IAuthenticationService _authenticationService;

        public AuthController(IMapper mapper, IAuthenticationService authenticationService)
        {
            _mapper = mapper;
            _authenticationService = authenticationService;
        }

        [AllowAnonymous]
        [Route("/api/login")]
        [HttpPost]
        public async Task<IActionResult> LoginAsync([FromBody] UserCredentialsResource credentialsResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            var result = await _authenticationService.GenerateAccessTokenAsync(credentialsResource.Username, credentialsResource.Password);

            if (!result.Success)
                return BadRequest(result.Message);

            var accessTokenResource = _mapper.Map<AccessToken, AccessTokenResource>(result.Resource);
            SetAuthCookie(accessTokenResource.RefreshToken);

            return Ok(accessTokenResource);
        }

        [Route("/api/token/refresh")]
        [HttpPost]
        public async Task<IActionResult> RefreshTokenAsync([FromBody] RefreshTokenResource refreshTokenResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            var result = await _authenticationService.RefreshTokenAsync(refreshTokenResource.Token, refreshTokenResource.Username);

            if (!result.Success)
                return BadRequest(result.Message);

            var tokenResource = _mapper.Map<AccessToken, AccessTokenResource>(result.Resource);
            return Ok(tokenResource);
        }

        [Route("/api/token/revoke")]
        [HttpPost]
        public IActionResult RevokeToken([FromBody] RevokeTokenResource revokeTokenResource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            _authenticationService.RevokeRefreshToken(revokeTokenResource.Token);
            return Ok();
        }

        private void SetAuthCookie(string token)
        {
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddDays(1)
            };
            Response.Cookies.Append("refreshToken", token, cookieOptions);
        }
    }
}
