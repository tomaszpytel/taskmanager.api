﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskManager.Api.Domain.Models.TaskManagerModel;

namespace TaskManager.Api.Persistence.Contexts
{
    public class TaskManagerContext : DbContext
    {
        public TaskManagerContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<TaskBoard> TaskBoards { get; set; }
        public DbSet<Assignment> Assignments { get; set; }

    }
}
