﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Repositories;
using TaskManager.Api.Persistence.Contexts;

namespace TaskManager.Api.Persistence.Repositories
{
    public class TaskBoardsRepository : BaseRepository, ITaskBoardsRepository
    {
        public TaskBoardsRepository(TaskManagerContext context) : base(context)
        {
        }

        public async Task AddAsync(TaskBoard taskBoard)
        {
            await _context.TaskBoards.AddAsync(taskBoard);
        }

        public async Task<TaskBoard> FindByIdAsync(int id)
        {
            return await _context.TaskBoards.FindAsync(id);
        }

        public async Task<IEnumerable<TaskBoard>> ListAsync(int userId)
        {
            return await _context.TaskBoards
                .Where(t => t.UserId == userId)
                .ToListAsync();
        }

        public void Remove(TaskBoard taskBoard)
        {
            _context.TaskBoards.Remove(taskBoard);
        }

        public void Update(TaskBoard taskBoard)
        {
            _context.TaskBoards.Update(taskBoard);
        }
    }
}
