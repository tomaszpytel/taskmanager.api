﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Repositories;
using TaskManager.Api.Persistence.Contexts;

namespace TaskManager.Api.Persistence.Repositories
{
    public class AssignmentsRepository : BaseRepository, IAssignmentsRepository
    {
        public AssignmentsRepository(TaskManagerContext context) : base(context)
        {
        }

        public async Task AddAsync(Assignment assignment)
        {
            await _context.Assignments.AddAsync(assignment);
        }

        public async Task<Assignment> FindByIdAsync(int id)
        {
            return await _context.Assignments.FindAsync(id);
        }

        public async Task<IEnumerable<Assignment>> ListAsync(int taskBoardId)
        {
            return await _context.Assignments
                .Where(a => a.TaskBoardId == taskBoardId)
                .ToListAsync();
        }

        public void Remove(Assignment assignment)
        {
            _context.Assignments.Remove(assignment);
        }

        public void Update(Assignment assignment)
        {
            _context.Assignments.Update(assignment);
        }
    }
}
