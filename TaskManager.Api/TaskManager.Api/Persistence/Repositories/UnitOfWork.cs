﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Repositories.UnitOfWork;
using TaskManager.Api.Persistence.Contexts;

namespace TaskManager.Api.Persistence.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TaskManagerContext _context;

        public UnitOfWork(TaskManagerContext context)
        {
            _context = context;
        }

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
