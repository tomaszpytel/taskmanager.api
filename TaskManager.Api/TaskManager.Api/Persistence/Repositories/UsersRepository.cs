﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Repositories;
using TaskManager.Api.Persistence.Contexts;

namespace TaskManager.Api.Persistence.Repositories
{
    public class UsersRepository : BaseRepository, IUserRepository
    {
        public UsersRepository(TaskManagerContext context) : base(context)
        {
        }

        public async Task AddAsync(User user)
        {
            await _context.Users.AddAsync(user);
        }

        public async Task<User> FindByIdAsync(int id)
        {
            return await _context.Users.FindAsync(id);
        }

        public async Task<IEnumerable<User>> ListAsync()
        {
            return await _context.Users
                .AsNoTracking()
                .ToListAsync();
        }

        public void Remove(User user)
        {
            _context.Users.Remove(user);
        }

        public void Update(User user)
        {
            _context.Users.Update(user);
        }

        public async Task<User> FindByUsernameAsync(string username)
        {
            return await _context.Users.
                SingleOrDefaultAsync(user => user.Username == username);
        }
    }
}
