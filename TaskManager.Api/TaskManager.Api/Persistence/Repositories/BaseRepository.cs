﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Persistence.Contexts;

namespace TaskManager.Api.Persistence.Repositories
{
    public abstract class BaseRepository
    {
        protected readonly TaskManagerContext _context;
        
        public BaseRepository(TaskManagerContext context)
        {
            _context = context;
        }
    }
}
