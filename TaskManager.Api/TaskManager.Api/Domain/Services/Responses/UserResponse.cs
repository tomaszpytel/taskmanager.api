﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;

namespace TaskManager.Api.Domain.Services.Responses
{
    public class UserResponse : BaseServiceResponse<User>
    {
        public UserResponse(User resource) : base(resource)
        {
        }

        public UserResponse(string message) : base(message)
        {
        }
    }
}
