﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Api.Domain.Services.Responses
{
    public abstract class BaseServiceResponse<T>
    {
        public bool Success { get; protected set; }
        public string Message { get; protected set; }
        public T Resource { get; protected set; }

        protected BaseServiceResponse(T resource)
        {
            Success = true;
            Message = string.Empty;
            Resource = resource;
        }

        protected BaseServiceResponse(string message)
        {
            Success = false;
            Message = message;
            Resource = default;
        }
    }
}
