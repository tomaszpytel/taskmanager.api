﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;

namespace TaskManager.Api.Domain.Services.Responses
{
    public class TaskBoardResponse : BaseServiceResponse<TaskBoard>
    {
        public TaskBoardResponse(TaskBoard resource) : base(resource)
        {
        }

        public TaskBoardResponse(string message) : base(message)
        {
        }
    }
}
