﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;

namespace TaskManager.Api.Domain.Services.Responses
{
    public class AssignmentResponse : BaseServiceResponse<Assignment>
    {
        public AssignmentResponse(Assignment resource) : base(resource)
        {
        }

        public AssignmentResponse(string message) : base(message)
        {
        }
    }
}
