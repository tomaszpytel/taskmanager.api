﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Security;

namespace TaskManager.Api.Domain.Services.Responses
{
    public class TokenResponse : BaseServiceResponse<AccessToken>
    {
        public TokenResponse(AccessToken resource) : base(resource)
        {
        }

        public TokenResponse(string message) : base(message)
        {
        }
    }
}
