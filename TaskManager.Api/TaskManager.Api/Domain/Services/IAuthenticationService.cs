﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Services.Responses;

namespace TaskManager.Api.Domain.Services
{
    public interface IAuthenticationService
    {
        Task<TokenResponse> GenerateAccessTokenAsync(string username, string password);
        Task<TokenResponse> RefreshTokenAsync(string refreshToken, string username);
        void RevokeRefreshToken(string refreshToken);
    }
}
