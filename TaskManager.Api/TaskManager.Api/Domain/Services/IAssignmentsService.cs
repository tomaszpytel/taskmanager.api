﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Services.Responses;

namespace TaskManager.Api.Domain.Services
{
    public interface IAssignmentsService
    {
        Task<IEnumerable<Assignment>> ListAsync(int TaskBoardId);
        Task<AssignmentResponse> AddAsync(Assignment assignment);
        Task<AssignmentResponse> UpdateAsync(int id, Assignment assignment);
        Task<AssignmentResponse> DeleteAsync(int id);
    }
}
