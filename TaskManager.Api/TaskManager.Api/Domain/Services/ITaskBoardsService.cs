﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Services.Responses;

namespace TaskManager.Api.Domain.Services
{
    public interface ITaskBoardsService
    {
        Task<IEnumerable<TaskBoard>> ListAsync(int userId);
        Task<TaskBoardResponse> AddAsync(TaskBoard taskBoard);
        Task<TaskBoardResponse> UpdateAsync(int id, TaskBoard taskBoard);
        Task<TaskBoardResponse> DeleteAsync(int id);
    }
}
