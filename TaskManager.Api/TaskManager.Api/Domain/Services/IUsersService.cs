﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;
using TaskManager.Api.Domain.Services.Responses;

namespace TaskManager.Api.Domain.Services
{
    public interface IUsersService
    {
        Task<IEnumerable<User>> ListAsync();
        Task<UserResponse> AddAsync(User user);
        Task<UserResponse> UpdateAsync(int id, User user);
        Task<UserResponse> DeleteAsync(int id);
        Task<User> FindByUsernameAsync(string username);
    }
}
