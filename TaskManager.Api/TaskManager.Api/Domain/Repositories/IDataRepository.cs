﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Api.Domain.Repositories
{
    public interface IDataRepository<TEntity>
    {
        Task<IEnumerable<TEntity>> ListAsync();
        Task AddAsync(TEntity entity);
        Task<TEntity> FindByIdAsync(int id);
        void Remove(TEntity entity);
        void Update(TEntity entity);
    }
}
