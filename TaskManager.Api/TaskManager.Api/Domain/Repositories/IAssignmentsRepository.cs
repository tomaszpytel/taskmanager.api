﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;

namespace TaskManager.Api.Domain.Repositories
{
    public interface IAssignmentsRepository
    {
        Task<IEnumerable<Assignment>> ListAsync(int taskBoardId);
        Task AddAsync(Assignment assignment);
        Task<Assignment> FindByIdAsync(int id);
        void Remove(Assignment assignment);
        void Update(Assignment assignment);
    }
}
