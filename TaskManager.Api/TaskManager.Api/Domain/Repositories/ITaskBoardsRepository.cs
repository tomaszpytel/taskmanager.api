﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaskManager.Api.Domain.Models.TaskManagerModel;

namespace TaskManager.Api.Domain.Repositories
{
    public interface ITaskBoardsRepository
    {
        Task<IEnumerable<TaskBoard>> ListAsync(int userId);
        Task AddAsync(TaskBoard taskBoard);
        Task<TaskBoard> FindByIdAsync(int id);
        void Remove(TaskBoard taskBoard);
        void Update(TaskBoard taskBoard);
    }
}
