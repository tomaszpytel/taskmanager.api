﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Api.Domain.Models.TaskManagerModel
{
    [Table("TaskBoards")]
    public class TaskBoard
    {
        [Key]
        public int TaskBoardId { get; set; }
        [ForeignKey("Users")]
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public virtual ICollection<Assignment> Assignments { get; set; }
        public virtual User User { get; set; }
    }
}
