﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Api.Domain.Models.TaskManagerModel
{
    [Table("Assignments")]
    public class Assignment
    {
        [Key]
        public int AssignmentId { get; set; }
        [ForeignKey("TaskBoards")]
        public int TaskBoardId { get; set; }
        public string Title { get; set; }
        public string Decsription { get; set; }
        public virtual TaskBoard TaskBoard { get; set; }
    }
}
