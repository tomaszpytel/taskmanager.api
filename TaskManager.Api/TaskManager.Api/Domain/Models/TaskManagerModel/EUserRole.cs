﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Api.Domain.Models.TaskManagerModel
{
    public enum EUserRole
    {
        Default = 1,
        Admin = 2
    }
}
