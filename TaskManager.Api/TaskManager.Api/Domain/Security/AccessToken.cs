﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Api.Domain.Security
{
    public class AccessToken : JsonWebToken
    {
        public string Username { get; set; }
        public RefreshToken RefreshToken { get; private set; }
        public AccessToken(string token, long expiration, RefreshToken refreshToken) : base(token, expiration)
        {
            RefreshToken = refreshToken ?? throw new ArgumentException("Refresh token not provided");
        }
    }
}
