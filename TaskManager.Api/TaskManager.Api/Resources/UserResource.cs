﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Api.Resources
{
    public class UserResource
    {
        public int UserId { get; set; }
        public string Username { get; set; }
    }
}
