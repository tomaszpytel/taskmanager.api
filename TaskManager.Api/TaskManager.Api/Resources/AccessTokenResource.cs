﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace TaskManager.Api.Resources
{
    public class AccessTokenResource
    {
        public string AccessToken { get; set; }
        public string Username { get; set; }
        public long Expiration { get; set; }
        [JsonIgnore]
        public string RefreshToken { get; set; }
    }
}
