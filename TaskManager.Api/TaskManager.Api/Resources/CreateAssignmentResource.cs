﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Api.Resources
{
    public class CreateAssignmentResource
    {
        [Required]
        public int TaskBoardId { get; set; }
        [Required]
        public string Title { get; set; }
        public string Decsription { get; set; }
    }
}
