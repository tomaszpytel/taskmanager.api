﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Api.Resources
{
    public class AssignmentResource
    {
        public int AssignmentId { get; set; }
        public string Title { get; set; }
        public string Decsription { get; set; }
    }
}
