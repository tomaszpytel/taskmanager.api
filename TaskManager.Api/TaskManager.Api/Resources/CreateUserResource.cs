﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Api.Resources
{
    public class CreateUserResource
    {
        [Required]
        [MaxLength(40)]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
