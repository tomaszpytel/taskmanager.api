﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManager.Api.Resources
{
    public class TaskBoardResource
    {
        public int TaskBoardId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
