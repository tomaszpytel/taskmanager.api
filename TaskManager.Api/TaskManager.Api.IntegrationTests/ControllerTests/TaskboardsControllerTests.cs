﻿using FluentAssertions;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using TaskManager.Api.Resources;
using Xunit;

namespace TaskManager.Api.IntegrationTests.ControllerTests
{
    public class TaskboardsControllerTests : IntegrationTest
    {
        [Fact]
        public async Task ListAsync_ReturnsEpmtyResponse()
        {
            //Arrange
            await AuthenticateAsync();

            //Act
            var response = await _httpClient.GetAsync(ApiRoutes.TaskBoards.ListAsync);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            (await response.Content.ReadAsAsync<IEnumerable<TaskBoardResource>>()).Should().BeEmpty();
        }

        [Fact]
        public async Task CreateAsync_ReturnsTaskBoardResourceResponse()
        {
            //Arrange
            await AuthenticateAsync();

            var testTaskBoard = new CreateTaskBoardResource
            {
                Title = "Test",
                Description = "Test"
            };

            //Act
            var response = await _httpClient.PostAsJsonAsync(ApiRoutes.TaskBoards.CreateAsync, testTaskBoard);

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            (await response.Content.ReadAsAsync<TaskBoardResource>()).Should().NotBeNull();
        }
    }
}
