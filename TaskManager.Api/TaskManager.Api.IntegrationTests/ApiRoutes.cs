﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManager.Api.IntegrationTests
{
    public static class ApiRoutes
    {
        private static readonly string _baseUrl = "https://localhost:44352/api/";
        public static class Auth
        {
            public static readonly string Login = string.Concat(_baseUrl, "login");

            public static readonly string RefreshToken = string.Concat(_baseUrl, "token/refresh");

            public static readonly string RevokeToken = string.Concat(_baseUrl, "token/revoke");
        }

        public static class Users
        {
            private static readonly string _controllerUrl = string.Concat(_baseUrl, "Users");

            public static readonly string ListAsync, CreateAsync, DeleteAsync = _controllerUrl;

            public static readonly string UpdateAsync = string.Concat(_controllerUrl, "/{userId}");
        }


        public static class TaskBoards
        {
            private static readonly string _controllerUrl = string.Concat(_baseUrl, "Taskboards");

            public static readonly string ListAsync = _controllerUrl;

            public static readonly string CreateAsync = _controllerUrl;

            public static readonly string UpdateAsync = string.Concat(_baseUrl, "/{taskbardId}");
        }

        public static class Assignments
        {
            private static readonly string _controllerUrl = string.Concat(_baseUrl, "Assignments");

            public static readonly string ListAsync, CreateAsync, DeleteAsync = _controllerUrl;

            public static readonly string UpdateAsync = string.Concat(_baseUrl, "/{assignmentId}");
        }
    }
}
