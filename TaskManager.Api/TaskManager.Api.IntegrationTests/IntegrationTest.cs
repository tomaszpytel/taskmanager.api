﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using TaskManager.Api.Persistence.Contexts;
using TaskManager.Api.Resources;

namespace TaskManager.Api.IntegrationTests
{
    public class IntegrationTest
    {
        protected readonly HttpClient _httpClient;

        protected IntegrationTest()
        {
            var appFactory = new WebApplicationFactory<Startup>()
                .WithWebHostBuilder(builder =>
                {
                    builder.ConfigureServices(services =>
                    {
                        services.RemoveAll(typeof(TaskManagerContext));
                        services.AddDbContext<TaskManagerContext>(options =>
                        {
                            options.UseInMemoryDatabase(Guid.NewGuid().ToString());
                        });
                    });
                });

            _httpClient = appFactory.CreateClient();
        }

        protected async Task AuthenticateAsync()
        {
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", await GetJwtAsync());
        }

        private async Task<string> GetJwtAsync()
        {
            var testUser = new CreateUserResource
            {
                Username = "testUser",
                Password = "testPassword"
            };

            await _httpClient.PostAsJsonAsync(ApiRoutes.Users.CreateAsync, testUser);

            var authResponse = await _httpClient.PostAsJsonAsync(ApiRoutes.Auth.Login, new UserCredentialsResource
            {
                Username = testUser.Username,
                Password = testUser.Password
            });

            var accessTokenResource =  await authResponse.Content.ReadAsAsync<AccessTokenResource>();
            return accessTokenResource.AccessToken;
        }
    }
}
